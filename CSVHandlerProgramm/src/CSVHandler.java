/**
 * @author dariush
 *
 */
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;
import java.io.FileReader;
import java.io.IOException;

public class CSVHandler {
  
  private String file =       "studentNameCSV.csv";           //muss sich im aktuellen Ordner befinden!
  private String delimiter =    ";";
  private String ersteZeile = "";
  // Constructor 1
  public CSVHandler() {
  }
  
  // Constructor 2
  public CSVHandler(String delimiter, String file) {
    super();
    this.delimiter = delimiter;
    this.file = file;
  }
  
  
  // Begin Methods
  
  public List<Schueler> getAll() {
    List<Schueler> students = new ArrayList<Schueler>();
    String zeile = "";
    System.out.println("Erste Aufgabe");
    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
      ersteZeile = reader.readLine().replaceAll(";", " ");
      while((zeile = reader.readLine()) != null){
        System.out.println(zeile);
        String[] contents = zeile.split(delimiter);
        students.add(new Schueler(contents[0]+" "+contents[1],Integer.parseInt(contents[2]),Integer.parseInt(contents[3]),Integer.parseInt(contents[4])));
      }
      
    } catch (IOException e) {
      e.printStackTrace();
    }
    
    return students;
  }
  public void printAll(List<Schueler> students) {
    System.out.println();
    System.out.println("Zweite & Dritte Aufgabe");
    System.out.println(ersteZeile);
    for (Schueler s : students) {
      System.out.print(s.getName() + " ");
      System.out.print(s.getJoker() + " ");
      System.out.print(s.getBlamiert() + " ");
      System.out.println(s.getFragen() + " ");
    }
  }
}